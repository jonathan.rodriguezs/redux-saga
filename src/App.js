import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import './App.css'
import { usersActions } from './redux/actions/usersActions'
import { Field, reduxForm } from 'redux-form'
import { filterCharacters } from './redux/actions/filterActions'
import Filter from './components/Filter/Filter'

function App({ filter, users, ...props }) {
  useEffect(() => {
    props.loadAllUsers()
  }, [users])

  return (
    <div className="App">
      <div className="App-header">
        <Filter />
        {users.map(u => (
          <Character key={u.name} {...u} />
        ))}
      </div>
    </div>
  )
}

export function getState({ users, filter }) {
  return {
    users: filterCharacters(users.list, filter),
    male: users.list.filter(u => u.gender === 'male').length,
    female: users.list.filter(u => u.gender === 'female').length,
    other: users.list.filter(u => u.gender === 'n/a').length,
    all: users.list.length,
    filter
  }
}

const mapStateToProps = state => getState(state)

const mapDispatchToProps = {
  loadAllUsers: usersActions.loadAllUsersRequest
}

const AppConnected = reduxForm({ form: 'App' })(App)

export default connect(mapStateToProps, mapDispatchToProps)(AppConnected)

function Character({ name, gender }) {
  return (
    <div>
      <h3>{name}</h3>
      <h5>{gender}</h5>
    </div>
  )
}

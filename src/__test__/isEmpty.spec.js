import { isEmpty, isEmptyObject } from './../util/helpers'

describe('isEmpty function', () => {
  test('value null should be true', () => {
    const valOfNull = null
    expect(isEmpty(valOfNull)).toBeTruthy()
  })

  test('array with length 0 should be true', () => {
    const emptyArray = []
    expect(isEmpty(emptyArray)).toBeTruthy()
  })

  test('array with elements should be false', () => {
    const emptyArray = [1, 2, 3]
    expect(isEmpty(emptyArray)).toBeFalsy()
  })
})

describe('isEmptyObject function', () => {
  test('object with empty props should be true ', () => {
    const obj = { name: '', age: null }
    expect(isEmptyObject(obj)).toBeTruthy()
  })
  test('object with with props not empty should be false', () => {
    const obj = { name: 'this is the name' }
    expect(isEmptyObject(obj)).toBeFalsy()
  })
})

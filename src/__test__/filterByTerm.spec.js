import { filterByTerm } from './../util/filterByTerm'

// (1)
// Every time you start writing a new suite of tests
// for a functionality wrap it in a "describe" block.
describe('Filter function', () => {
  // (2)
  // the "test" function is the actual test block.

  const input = [
    { id: 1, url: 'https://www.url1.dev' },
    { id: 2, url: 'https://www.url2.dev' },
    { id: 3, url: 'https://www.link3.dev' }
  ]

  test('it should filter by a search term (link) ', () => {
    const input = [
      { id: 1, url: 'https://www.url1.dev' },
      { id: 2, url: 'https://www.url2.dev' },
      { id: 3, url: 'https://www.link3.dev' }
    ]

    const output = [{ id: 3, url: 'https://www.link3.dev' }]

    expect(filterByTerm(input, 'link')).toEqual(output)
    expect(filterByTerm(input, 'LINK')).toEqual(output)
  })

  it('it should throw when searchTerm is empty string ', () => {
    expect(() => {
      filterByTerm(input)
    }).toThrowError('searchTerm cannot be empty')
  })

  it('it should throw when inputArr is empty string ', () => {
    expect(() => {
      filterByTerm()
    }).toThrowError('searchTerm cannot be empty')
  })
})

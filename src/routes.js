import Home from './components/Home/Home'

export const routes = [
  {
    name: 'Home',
    path: '/',
    component: Home
  }
]

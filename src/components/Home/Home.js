import React from 'react'
import { connect } from 'react-redux'

import Articles from './../Articles/Articles'

function Home(props) {
  return <Articles />
}

const mapStateToProps = state => ({})
const mapDispatchToProps = {}
export default connect(mapStateToProps, mapDispatchToProps)(Home)

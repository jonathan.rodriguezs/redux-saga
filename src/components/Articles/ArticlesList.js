import React from 'react'
import { connect } from 'react-redux'
import { articleActions } from './articlesRedux'

export function Article({ id, article, deleteArticle, editArticle, selectArticle, ...props }) {
  function handleClickUpdate() {
    props.setShow(true)
    editArticle(id)
  }
  return (
    <li key={id}>
      {article}
      <button onClick={() => deleteArticle(id)}> delete</button>
      <button onClick={handleClickUpdate}> edit</button>
      <button onClick={() => selectArticle(id)}> select</button>
    </li>
  )
}

function ArticlesList({ articles, ...props }) {
  return (
    <ul>
      {articles.map(article => (
        <Article key={article.id} {...article} {...props} />
      ))}
    </ul>
  )
}

const mapStateToProps = state => ({
  articles: state.articles
})

const mapDispatchToProps = {
  deleteArticle: articleActions.deleteArticlesSuccess,
  editArticle: articleActions.editArticlesSuccess,
  selectArticle: articleActions.selectArticlesSuccess
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesList)

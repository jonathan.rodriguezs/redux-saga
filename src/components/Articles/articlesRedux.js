import { generateId } from './../../util/crud'
import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  // LOAD
  loadArticlesRequest: ['payload'],
  loadArticlesSuccess: ['payload'],
  loadArticlesFailure: ['payload'],
  // CREATE
  createArticlesRequest: ['payload'],
  createArticlesSuccess: ['payload'],
  createArticlesFailure: ['payload'],
  // EDIT
  editArticlesRequest: ['payload'],
  editArticlesSuccess: ['payload'],
  editArticlesFailure: ['payload'],
  // SELECT
  selectArticlesRequest: ['payload'],
  selectArticlesSuccess: ['payload'],
  selectArticlesFailure: ['payload'],
  // UPDATE
  updateArticlesRequest: ['payload'],
  updateArticlesSuccess: ['payload'],
  updateArticlesFailure: ['payload'],
  // DELETE
  deleteArticlesRequest: ['payload'],
  deleteArticlesSuccess: ['payload'],
  deleteArticlesFailure: ['payload']
})

export const loadArticlesSuccess = (state, action) => {
  return [...state, { id: generateId(), article: action.payload }]
}

export const updateArticlesSuccess = (state, action) => {
  return [...state.filter(article => article.id !== action.payload.id), action.payload]
}

export const editArticlesSuccess = (state, action) => {
  return state.map(article =>
    article.id === action.payload ? { ...article, edit: true } : { ...article, edit: false }
  )
}

export const selectArticlesSuccess = (state, action) => {
  return state.map(article =>
    article.id === action.payload ? { ...article, selected: true } : { ...article, selected: false }
  )
}

export const deleteArticlesSuccess = (state, action) => {
  return state.filter(article => article.id !== action.payload)
}

export const HANDLERS = {
  // LOAD
  [Types.LOAD_ARTICLES_REQUEST]: null,
  [Types.LOAD_ARTICLES_SUCCESS]: loadArticlesSuccess,
  [Types.LOAD_ARTICLES_FAILURE]: null,
  // CREATE
  [Types.CREATE_ARTICLES_REQUEST]: null,
  [Types.CREATE_ARTICLES_SUCCESS]: null,
  [Types.CREATE_ARTICLES_FAILURE]: null,
  // EDIT
  [Types.EDIT_ARTICLES_REQUEST]: null,
  [Types.EDIT_ARTICLES_SUCCESS]: editArticlesSuccess,
  [Types.EDIT_ARTICLES_FAILURE]: null,
  // UPDATE
  [Types.UPDATE_ARTICLES_REQUEST]: null,
  [Types.UPDATE_ARTICLES_SUCCESS]: updateArticlesSuccess,
  [Types.UPDATE_ARTICLES_FAILURE]: null,
  // SELECT
  [Types.SELECT_ARTICLES_REQUEST]: null,
  [Types.SELECT_ARTICLES_SUCCESS]: selectArticlesSuccess,
  [Types.SELECT_ARTICLES_FAILURE]: null,
  // DELETE
  [Types.DELETE_ARTICLES_REQUEST]: null,
  [Types.DELETE_ARTICLES_SUCCESS]: deleteArticlesSuccess,
  [Types.DELETE_ARTICLES_FAILURE]: null
}
const INITIAL_STATE = []

export const articleTypes = Types
export const articleActions = Creators
export const articles = createReducer(INITIAL_STATE, HANDLERS)

import React, { useState } from 'react'
import ArticlesList from './ArticlesList'
import ArticlesForm from './ArticlesForm'
import Article from './Article'

function Articles() {
  const [add, setAdd] = useState(false)

  return (
    <>
      <h1>List Of Articles</h1>
      <button type='button' disabled={add} onClick={() => setAdd(!add)}>
        +
      </button>
      {add && <ArticlesForm setShow={setAdd} />}
      <ArticlesList setShow={setAdd} />
      <Article />
    </>
  )
}

export default Articles

import React from 'react'
import { connect } from 'react-redux'
import { reduxForm, Form } from 'redux-form'
import { articleActions } from './../Articles/articlesRedux'
import { Input } from './../Inputs/Inputs'
import { isEmpty } from './../../util/helpers'

function ArticlesForm({ handleSubmit, pristine, submitting, initialValues, reset, ...props }) {
  function handleClickSubmit(values) {
    if (values.edit) {
      const { id, article } = values
      const payload = { id, article }
      props.update(payload)
    } else {
      props.create(values.article)
    }
    reset()
    props.setShow(false)
  }

  function handleClickCancel() {
    props.initialize({})
    props.setShow(false)
  }

  return (
    <Form onSubmit={handleSubmit(handleClickSubmit)}>
      <div>
        <label htmlFor='article'>Article :</label>
        <Input name='article' type='text' />
      </div>
      <button type='submit' disabled={pristine || submitting}>
        {isEmpty(initialValues) ? 'Submit' : 'Update'}
      </button>

      <button type='button' disabled={pristine || submitting} onClick={reset}>
        Undo Changes
      </button>

      <button type='button' onClick={handleClickCancel}>
        Cancel
      </button>
    </Form>
  )
}

const mapStateToProps = state => {
  return {
    initialValues: state['articles'].find(a => a.edit === true)
  }
}
const mapDispatchToProps = {
  create: articleActions.loadArticlesSuccess,
  update: articleActions.updateArticlesSuccess
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'article',
    enableReinitialize: true
  })(ArticlesForm)
)

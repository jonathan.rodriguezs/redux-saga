import React from 'react'
import { connect } from 'react-redux'
import { articleActions } from './../Articles/articlesRedux'
import { Input } from './../Inputs/Inputs'
import { isEmpty } from './../../util/helpers'

function Article({ article }) {
  return <pre>{JSON.stringify(article, ' ', 2)}</pre>
}

const mapStateToProps = state => {
  return {
    article: state['articles'].find(a => a.selected === true)
  }
}
const mapDispatchToProps = {
  create: articleActions.loadArticlesSuccess,
  update: articleActions.updateArticlesSuccess
}

export default connect(mapStateToProps, mapDispatchToProps)(Article)

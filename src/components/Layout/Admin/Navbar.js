import React from 'react'
import PropTypes from 'prop-types'

function Navbar(props) {
  return (
    <React.Fragment>
      {/* Topbar Start */}
      <div className='navbar-custom'>
        <ul className='list-unstyled topnav-menu float-right mb-0'>
          <li className='d-none d-sm-block'>
            <form className='app-search'>
              <div className='app-search-box'>
                <div className='input-group'>
                  <input type='text' className='form-control' placeholder='Search...' />
                  <div className='input-group-append'>
                    <button className='btn' type='submit'>
                      <i className='fe-search' />
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </li>
          <li className='dropdown notification-list'>
            <a
              className='nav-link dropdown-toggle waves-light waves-effect'
              data-toggle='dropdown'
              href='#'
              role='button'
              aria-haspopup='false'
              aria-expanded='false'>
              <i className='fe-bell noti-icon' />
              <span className='badge badge-danger rounded-circle noti-icon-badge'>5</span>
            </a>
            <div className='dropdown-menu dropdown-menu-right dropdown-lg' style={{}}>
              {/* item*/}
              <div className='dropdown-item noti-title'>
                <h5 className='m-0 text-white'>
                  <span className='float-right'>
                    <a href='#' className='text-white'>
                      <small>Clear All</small>
                    </a>
                  </span>
                  Notification
                </h5>
              </div>
              <div
                className='slimScrollDiv'
                style={{
                  position: 'relative',
                  overflow: 'hidden',
                  width: 'auto',
                  height: 491
                }}>
                <div className='slimscroll noti-scroll' style={{ overflow: 'hidden', width: 'auto', height: 491 }}>
                  {/* item*/}
                  <a href='' className='dropdown-item notify-item active'>
                    <div className='notify-icon'>
                      <img src='assets/images/users/user-1.jpg' className='img-fluid rounded-circle' alt />{' '}
                    </div>
                    <p className='notify-details'>Cristina Pride</p>
                    <p className='text-muted mb-0 user-msg'>
                      <small>Hi, How are you? What about our next meeting</small>
                    </p>
                  </a>
                  {/* item*/}
                  <a href='' className='dropdown-item notify-item'>
                    <div className='notify-icon bg-primary'>
                      <i className='mdi mdi-comment-account-outline' />
                    </div>
                    <p className='notify-details'>
                      Caleb Flakelar commented on Admin
                      <small className='text-muted'>1 min ago</small>
                    </p>
                  </a>
                  {/* item*/}
                  <a href='' className='dropdown-item notify-item'>
                    <div className='notify-icon'>
                      <img src='assets/images/users/user-4.jpg' className='img-fluid rounded-circle' alt />{' '}
                    </div>
                    <p className='notify-details'>Karen Robinson</p>
                    <p className='text-muted mb-0 user-msg'>
                      <small>Wow ! this admin looks good and awesome design</small>
                    </p>
                  </a>
                  {/* item*/}
                  <a href='' className='dropdown-item notify-item'>
                    <div className='notify-icon bg-warning'>
                      <i className='mdi mdi-account-plus' />
                    </div>
                    <p className='notify-details'>
                      New user registered.
                      <small className='text-muted'>5 hours ago</small>
                    </p>
                  </a>
                  {/* item*/}
                  <a href='' className='dropdown-item notify-item'>
                    <div className='notify-icon bg-info'>
                      <i className='mdi mdi-comment-account-outline' />
                    </div>
                    <p className='notify-details'>
                      Caleb Flakelar commented on Admin
                      <small className='text-muted'>4 days ago</small>
                    </p>
                  </a>
                  {/* item*/}
                  <a href='' className='dropdown-item notify-item'>
                    <div className='notify-icon bg-secondary'>
                      <i className='mdi mdi-heart text-danger' />
                    </div>
                    <p className='notify-details'>
                      Carlos Crouch liked
                      <b>Admin</b>
                      <small className='text-dark'>13 days ago</small>
                    </p>
                  </a>
                </div>
                <div
                  className='slimScrollBar'
                  style={{
                    background: 'rgb(158, 165, 171)',
                    width: 8,
                    position: 'absolute',
                    top: 0,
                    opacity: '0.4',
                    display: 'block',
                    borderRadius: 7,
                    zIndex: 99,
                    right: 1
                  }}
                />
                <div
                  className='slimScrollRail'
                  style={{
                    width: 8,
                    height: '100%',
                    position: 'absolute',
                    top: 0,
                    display: 'none',
                    borderRadius: 7,
                    background: 'rgb(51, 51, 51)',
                    opacity: '0.2',
                    zIndex: 90,
                    right: 1
                  }}
                />
              </div>
              {/* All*/}
              <a href='' className='dropdown-item text-center text-primary notify-item notify-all'>
                View all
                <i className='fi-arrow-right' />
              </a>
            </div>
          </li>
          <li className='dropdown notification-list'>
            <a
              className='nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light'
              data-toggle='dropdown'
              href='#'
              role='button'
              aria-haspopup='false'
              aria-expanded='false'>
              <img src='assets/images/users/user-1.jpg' alt='user-image' className='rounded-circle' />
              <span className='pro-user-name ml-1'>
                Marcia J. <i className='mdi mdi-chevron-down' />
              </span>
            </a>
            <div className='dropdown-menu dropdown-menu-right profile-dropdown '>
              {/* item*/}
              <div className='dropdown-item noti-title'>
                <h5 className='m-0 text-white'>Welcome !</h5>
              </div>
              {/* item*/}
              <a href='' className='dropdown-item notify-item'>
                <i className='fe-user' />
                <span>My Account</span>
              </a>
              {/* item*/}
              <a href='' className='dropdown-item notify-item'>
                <i className='fe-settings' />
                <span>Settings</span>
              </a>
              {/* item*/}
              <a href='' className='dropdown-item notify-item'>
                <i className='fe-lock' />
                <span>Lock Screen</span>
              </a>
              <div className='dropdown-divider' />
              {/* item*/}
              <a href='' className='dropdown-item notify-item'>
                <i className='fe-log-out' />
                <span>Logout</span>
              </a>
            </div>
          </li>
          <li className='dropdown notification-list'>
            <a href='' className='nav-link right-bar-toggle waves-effect waves-light'>
              <i className='fe-settings noti-icon' />
            </a>
          </li>
        </ul>
        {/* LOGO */}
        <div className='logo-box'>
          <a href='index.html' className='logo text-center'>
            <span className='logo-lg'>
              <img src='assets/images/logo-light.png' alt height={24} />
              {/* <span class="logo-lg-text-light">Upvex</span> */}
            </span>
            <span className='logo-sm'>
              {/* <span class="logo-sm-text-dark">X</span> */}
              <img src='assets/images/logo-sm.png' alt height={28} />
            </span>
          </a>
        </div>
        <ul className='list-unstyled topnav-menu topnav-menu-left m-0'>
          <li>
            <button className='button-menu-mobile waves-effect waves-light'>
              <span />
              <span />
              <span />
            </button>
          </li>
          <li className='dropdown d-none d-lg-block'>
            <a
              className='nav-link dropdown-toggle waves-effect waves-light'
              data-toggle='dropdown'
              href='#'
              role='button'
              aria-haspopup='false'
              aria-expanded='false'>
              Reports
              <i className='mdi mdi-chevron-down' />
            </a>
            <div className='dropdown-menu'>
              {/* item*/}
              <a href='' className='dropdown-item'>
                Finance Report
              </a>
              {/* item*/}
              <a href='' className='dropdown-item'>
                Monthly Report
              </a>
              {/* item*/}
              <a href='' className='dropdown-item'>
                Revenue Report
              </a>
              {/* item*/}
              <a href='' className='dropdown-item'>
                Settings
              </a>
              {/* item*/}
              <a href='' className='dropdown-item'>
                Help &amp; Support
              </a>
            </div>
          </li>
          <li className='dropdown dropdown-mega d-none d-lg-block'>
            <a
              className='nav-link dropdown-toggle waves-effect waves-light'
              data-toggle='dropdown'
              href='#'
              role='button'
              aria-haspopup='false'
              aria-expanded='false'>
              Mega Menu
              <i className='mdi mdi-chevron-down' />
            </a>
            <div className='dropdown-menu dropdown-megamenu'>
              <div className='row'>
                <div className='col-sm-8'>
                  <div className='row'>
                    <div className='col-md-4'>
                      <h5 className='text-dark mt-0'>UI Components</h5>
                      <ul className='list-unstyled megamenu-list mt-2'>
                        <li>
                          <a href=''>Widgets</a>
                        </li>
                        <li>
                          <a href=''>Nestable List</a>
                        </li>
                        <li>
                          <a href=''>Range Sliders</a>
                        </li>
                        <li>
                          <a href=''>Masonry Items</a>
                        </li>
                        <li>
                          <a href=''>Sweet Alerts</a>
                        </li>
                        <li>
                          <a href=''>Treeview Page</a>
                        </li>
                        <li>
                          <a href=''>Tour Page</a>
                        </li>
                      </ul>
                    </div>
                    <div className='col-md-4'>
                      <h5 className='text-dark mt-0'>Applications</h5>
                      <ul className='list-unstyled megamenu-list mt-2'>
                        <li>
                          <a href=''>Email Pages</a>
                        </li>
                        <li>
                          <a href=''>Profile</a>
                        </li>
                        <li>
                          <a href=''>Calendar</a>
                        </li>
                        <li>
                          <a href=''>Team Contacts</a>
                        </li>
                        <li>
                          <a href=''>Maintenance</a>
                        </li>
                        <li>
                          <a href=''>Coming Soon Page</a>
                        </li>
                      </ul>
                    </div>
                    <div className='col-md-4'>
                      <h5 className='text-dark mt-0'>Layouts</h5>
                      <ul className='list-unstyled megamenu-list mt-2'>
                        <li>
                          <a href=''>Small Sidebar</a>
                        </li>
                        <li>
                          <a href=''>Light Sidebar</a>
                        </li>
                        <li>
                          <a href=''>Dark Topbar</a>
                        </li>
                        <li>
                          <a href=''>Preloader</a>
                        </li>
                        <li>
                          <a href=''>Sidebar Collapsed</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className='col-sm-4'>
                  <div className='text-center mt-3'>
                    <h3 className='text-dark'>Launching Discount Sale!</h3>
                    <p className='font-16'>Save up to 55% off.</p>
                    <button className='btn btn-primary mt-1'>
                      Download Now <i className='mdi mdi-arrow-right-bold-outline ml-1' />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      {/* end Topbar */}
    </React.Fragment>
  )
}

Navbar.propTypes = {}

export default Navbar

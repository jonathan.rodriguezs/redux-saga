import React from 'react'

const PageTitle = props => {
  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='col-12'>
          <div className='page-title-box'>
            <div className='page-title-right'>
              <ol className='breadcrumb m-0'>
                <li className='breadcrumb-item'>
                  <a href='javascript: void(0);'>Upvex</a>
                </li>
                <li className='breadcrumb-item'>
                  <a href='javascript: void(0);'>Pages</a>
                </li>
                <li className='breadcrumb-item active'>Starter</li>
              </ol>
            </div>
            <h4 className='page-title'>Starter</h4>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PageTitle

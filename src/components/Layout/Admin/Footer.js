import React from 'react'
import PropTypes from 'prop-types'

const Footer = props => {
  return (
    <React.Fragment>
      {/* Footer Start */}
      <footer className='footer'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-6'>
              2019 © Upvex theme by <a href>Coderthemes</a>
            </div>
            <div className='col-md-6'>
              <div className='text-md-right footer-links d-none d-sm-block'>
                <a href='javascript:void(0);'>About Us</a>
                <a href='javascript:void(0);'>Help</a>
                <a href='javascript:void(0);'>Contact Us</a>
              </div>
            </div>
          </div>
        </div>
      </footer>
      {/* end Footer */}
    </React.Fragment>
  )
}

Footer.propTypes = {}

export default Footer

import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { routes } from './../../routes'

export const SideMenuLink = ({ name, path }) => (
  <li className='active'>
    <NavLink exact to={path} activeClassName='active'>
      <span>{name}</span>
    </NavLink>
  </li>
)

function SideMenu(props) {
  return (
    <React.Fragment>
      {/* ========== Left Sidebar Start ========== */}
      <div className='left-side-menu'>
        <div
          className='slimScrollDiv'
          style={{
            position: 'relative',
            overflow: 'hidden',
            width: 'auto',
            height: 553
          }}>
          <div className='slimscroll-menu' style={{ overflow: 'hidden', width: 'auto', height: 553 }}>
            {/*- Sidemenu */}
            <div id='sidebar-menu' className='active'>
              <ul className='metismenu in' id='side-menu'>
                <li className='menu-title'>Navigation</li>

                {routes.map((route, i) => (
                  <SideMenuLink key={i} {...route} />
                ))}
              </ul>
            </div>
            {/* End Sidebar */}
            <div className='clearfix' />
          </div>
          <div
            className='slimScrollBar'
            style={{
              background: 'rgb(158, 165, 171)',
              width: 8,
              position: 'absolute',
              top: '-201.25px',
              opacity: '0.4',
              display: 'none',
              borderRadius: 7,
              zIndex: 99,
              right: 1,
              height: '257.198px'
            }}
          />
          <div
            className='slimScrollRail'
            style={{
              width: 8,
              height: '100%',
              position: 'absolute',
              top: 0,
              display: 'none',
              borderRadius: 7,
              background: 'rgb(51, 51, 51)',
              opacity: '0.2',
              zIndex: 90,
              right: 1
            }}
          />
        </div>
        {/* Sidebar -left */}
      </div>
      {/* Left Sidebar End */}
    </React.Fragment>
  )
}

SideMenu.propTypes = {}

export default SideMenu

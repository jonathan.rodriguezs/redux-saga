import React from 'react'
import { connect } from 'react-redux'
import Navbar from './Navbar'
import Content from './Content'
import SideMenu from './SideMenu'
import Wrapper from './Wrapper'

function Layout(props) {
  return (
    <Wrapper>
      <Navbar />
      <SideMenu />
      <Content>{props.children}</Content>
    </Wrapper>
  )
}

const mapStateToProps = ({ loading }) => ({ loading })
const mapDispatchToProps = {}
export default connect(mapStateToProps, mapDispatchToProps)(Layout)

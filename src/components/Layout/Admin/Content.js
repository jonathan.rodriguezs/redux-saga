import React from 'react'
import PropTypes from 'prop-types'
import Footer from './Footer'
import PageTitle from './PageTitle'

function Content(props) {
  return (
    <div className='content-page'>
      <div className='content'>
        <PageTitle />
        <div className='container-fluid'>{props.children}</div>
      </div>
      <Footer />
    </div>
  )
}

Content.propTypes = {}

export default Content

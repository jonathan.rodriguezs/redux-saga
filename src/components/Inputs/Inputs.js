import React from 'react'
import { Field } from 'redux-form'

export const CustomInput = ({ input, label, type, placeholder, meta: { touched, error, warning }, ...props }) => {
  return (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} placeholder={placeholder} type={type} {...props} />
        {touched &&
          ((error && <span className='text-danger'>Error : {error}</span>) ||
            (warning && <span className='text-warning'> Warning : {warning}</span>))}
      </div>
    </div>
  )
}

export const InputClean = ({ input, type, placeholder, meta: { touched, error, warning }, ...props }) => {
  return (
    <>
      <input {...input} placeholder={placeholder} type={type} {...props} />
      {touched &&
        ((error && <span className='text-danger'>Error : {error}</span>) ||
          (warning && <span className='text-warning'> Warning : {warning}</span>))}
    </>
  )
}

export const FieldInputWrapper = (Field, Input, props) => <Field {...props} component={Input} />

export const InputField = props => FieldInputWrapper(Field, CustomInput, props)

export const Input = props => FieldInputWrapper(Field, InputClean, props)

const C = props => (
  <InputField
    name='email'
    type='email'
    className='form-control form-control-user'
    id='exampleInputEmail'
    aria-describedby='emailHelp'
    placeholder='Enter Email Address...'
  />
)

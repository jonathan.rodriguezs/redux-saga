import React, { useEffect } from 'react'
import { connect } from 'react-redux'
// import { reduxForm } from 'redux-form'
import Login from './../Login/Login'

function LoginContainer(props) {
  useEffect(() => {
    document.body.classList.add('bg-gradient-primary')
  })

  useEffect(() => {
    return () => {
      document.body.classList.remove('bg-gradient-primary')
    }
  })

  function submit(event) {
    debugger
  }

  return <Login {...props} onSubmit={submit} />
}

const mapStateToProps = ({ auth, request }) => ({
  auth,
  request
})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)

import React from 'react'
import { connect } from 'react-redux'

export const ComponentWithLayoutRedux = (Component, Layout, mapStateToProps, mapDispatchToProps) => {
  class Components extends React.Component {
    componentDidMount() {}

    componentWillUnmount() {}

    render() {
      return (
        <Layout>
          <Component />
        </Layout>
      )
    }
  }

  return connect(mapStateToProps, mapDispatchToProps)(Components)
}

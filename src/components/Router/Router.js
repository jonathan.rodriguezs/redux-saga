import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { history } from './../../redux/configStore'

export const Routes = ({ routes }) => routes.map((route, i) => <Route exact key={i} {...route} />)

function Router(props) {
  return (
    <ConnectedRouter history={history}>
      <BrowserRouter>
        <Switch>
          <Routes {...props} />
          <Route exact path='*' render={() => <h1>Not Found</h1>} />
        </Switch>
      </BrowserRouter>
    </ConnectedRouter>
  )
}

Router.propTypes = {}

export default Router

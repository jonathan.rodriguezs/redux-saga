import { createActions } from 'reduxsauce'

const { Creators } = createActions(
  {
    loadAllUsersRequest: ['payload'],
    loadAllUsersSuccess: ['payload'],
    loadAllUsersFailure: ['payload'],

    loadUsersRequest: ['payload'],
    loadUsersSuccess: ['payload'],
    loadUsersFailure: ['payload'],

    createUsersRequest: ['payload'],
    createUsersSuccess: ['payload'],
    createUsersFailure: ['payload'],

    deleteUsersRequest: ['payload'],
    deleteUsersSuccess: ['payload'],
    deleteUsersFailure: ['payload']
  },
  {}
)

export const usersActions = Creators

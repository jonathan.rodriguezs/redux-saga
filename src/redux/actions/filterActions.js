import { SHOW_ALL, SHOW_MALE, SHOW_OTHER, SHOW_FEMALE, SET_VISIBILITY_FILTER } from './../types/filterTypes'

export const showMale = () => ({
  type: SET_VISIBILITY_FILTER,
  payload: SHOW_MALE
})
export const showFemale = () => ({
  type: SET_VISIBILITY_FILTER,
  payload: SHOW_FEMALE
})
export const showAll = () => ({
  type: SET_VISIBILITY_FILTER,
  payload: SHOW_ALL
})
export const showOther = () => ({
  type: SET_VISIBILITY_FILTER,
  payload: SHOW_OTHER
})

export function filterCharacters(users, filter) {
  switch (filter) {
    case SHOW_ALL:
      return users
    case SHOW_MALE:
      return users.filter(u => u.gender === 'male')
    case SHOW_FEMALE:
      return users.filter(u => u.gender === 'female')
    case SHOW_OTHER:
      return users.filter(u => u.gender === 'n/a')
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

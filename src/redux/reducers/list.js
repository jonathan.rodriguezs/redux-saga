import { defaultState } from './../defaultState'

export const FILTER_ALL = 'FILTER_ALL'
export const FILTER_MALE = 'FILTER_MALE'
export const FILTER_FEMALE = 'FILTER_FEMALE'
export const FILTER_OTHER = 'FILTER_OTHER'

export function list(state = defaultState.items, action) {
  switch (action.type) {
    case FILTER_ALL:
      return {
        documents: action.payload,
        all: action.payload.length,
        male: action.payload.filter(u => u.gender === 'male').length,
        female: action.payload.filter(u => u.gender === 'female').length,
        other: action.payload.filter(u => u.gender === 'n/a').length
      }
    case FILTER_MALE:
      return {
        documents: action.payload.filter(u => u.gender === 'male'),
        all: action.payload.length,
        male: action.payload.filter(u => u.gender === 'male').length,
        female: action.payload.filter(u => u.gender === 'female').length,
        other: action.payload.filter(u => u.gender === 'n/a').length
      }
    case FILTER_FEMALE:
      return {
        documents: action.payload.filter(u => u.gender === 'female'),
        all: action.payload.length,
        male: action.payload.filter(u => u.gender === 'male').length,
        female: action.payload.filter(u => u.gender === 'female').length,
        other: action.payload.filter(u => u.gender === 'n/a').length
      }
    case FILTER_OTHER:
      return {
        documents: action.payload.filter(u => u.gender === 'n/a'),
        all: action.payload.length,
        male: action.payload.filter(u => u.gender === 'male').length,
        female: action.payload.filter(u => u.gender === 'female').length,
        other: action.payload.filter(u => u.gender === 'n/a').length
      }
    default:
      return state
  }
}

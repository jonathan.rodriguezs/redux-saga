import { defaultState } from './../defaultState'
import { SET_VISIBILITY_FILTER } from './../types/filterTypes'

export const filter = (state = defaultState.filter, action) => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.payload
    default:
      return state
  }
}

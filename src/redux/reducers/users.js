import { defaultState } from './../defaultState'
import { usersTypes } from './../types/usersTypes'

const userss = defaultState.users
export function users(state = userss, action) {
  switch (action.type) {
    case usersTypes.LOAD_USERS_REQUEST:
      return state
    case usersTypes.LOAD_ALL_USERS_SUCCESS:
      return { ...state, list: [...action.payload] }
    default:
      return state
  }
}

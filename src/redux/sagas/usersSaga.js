import axios from 'axios'
import { call, put, all, takeLatest, take, select } from 'redux-saga/effects'
import { usersTypes } from './../types/usersTypes'
import { usersActions } from './../actions/usersActions'

export function* loadAllUsers() {
  try {
    const baseUrl = 'https://swapi.co/api/people/'
    const response = yield call(axios.get, baseUrl)
    const payload = response.data.results
    const dispatch = usersActions.loadAllUsersSuccess(payload)
    // Dispatch
    yield put(dispatch)
  } catch (error) {
    console.log(error)
  }
}

export function* loadUser() {
  const _id = select(state => state.user.id)
  const baseUrl = 'https://swapi.co/api/people/'
  const response = yield call(axios.get, `${baseUrl}/${_id}`)
}

export function* usersSagas() {
  yield all([takeLatest(usersTypes.LOAD_ALL_USERS_REQUEST, loadAllUsers)])
}
